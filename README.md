# FameStation API
![Standard - JavaScript Style Guide](https://codeship.com/projects/eb103ec0-e354-0134-12a6-3ae0b9756505/status?branch=development)


[![Standard - JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

## Node version: 7.7.0

Downloading the project
You should have enough knowledge to clone this project if you're already reading this, but if you don't you can enter this in the terminal.

* ` $ git pull git@bitbucket.org:famestation/famestation-api.git`

## *Important notes aka setup instructions*

## Environment variables
First of all, you'll need to configure _node_ to run your own settings and such for best result and even to get the server up and running.

- Copy .env-template to 
  - .env to setup you development environment
  - .env-test to setup you test environment

### Database setup
This step will create two databases for you, one for development purposes and another one for the tests to be run on.

- You probaly want to install postgres first, as a mac user I did it via homebrew
- start postgres with `postgres -D path_to_config` (for me path_to_config was `/usr/local/var/postgres`)
- enter the postgres command line with the command `psql template1` (template1 is the standard system database in postgres)
- execute `CREATE USER famestation_user WITH PASSWORD 'AnyPasswordYouWwant';`
- execute `CREATE DATABASE famestation_db;`
- execute `CREATE DATABASE famestation_db_test;`
- execute `GRANT ALL PRIVILEGES ON DATABASE famestation_db to famestation_user;`
- execute `GRANT ALL PRIVILEGES ON DATABASE famestation_db_test to famestation_user;`
- execute `\q` to quit psql

### Run migrations
 Migrations are handled by the knex module, all of the migration should be runned when starting the server by default. As it's configured in package.json like that currently.

- Run migrations by `$ npm run migrate` in the terminal. Now you should have all the tables set up in the database.

### Start the API

* ` $ cd application`
* ` $ nvm use`
* ` $ npm install`
* ` $ npm run start-dev`

### Project Structure
```
- application (app src)
    \ config
    \ migrations
    \ seeds
    \ src (The server is here)
      \ controllers
      \ errors
      \ middlewares
      \ models
      \ responses
      \ routes
      \ services
      \ utils
      - app.js
      - server.js
    \ test
- build (docker stuff)
    \ nginx
    \ node
```

- **controllers** - Will just delegate the request to a service.
- **errors** - A neat little error function/class that you can throw.
- **middlewares** - These will be ran before getting passed down to a controller.
- **models** - Bookshelf(The ORM beeing used) models
- **responses** - Here are two helper wrappers around the res object for success and error responses.
- **routes** - This is where you define your routes and which middlewares the route should have.
- **services** - Here is where most of the business logic will be handled.
- **utils** - You can create utility modules here to import into your other modules.