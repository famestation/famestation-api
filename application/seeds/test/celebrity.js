const userTable = 'users'
const celebrityTable = 'celebrities'
const hasher = require('../../src/utils/hasher')

exports.seed = function (knex, Promise) {
  return hasher.hash('qweqwe')
  .then((hash) =>
    knex(userTable).insert({
      firstname: 'test',
      lastname: 'test',
      email: 'celebrity@test.com',
      password: hash,
      type: 'celebrity'
    })
    .returning('id')
  )
  .then((id) =>
    knex(celebrityTable)
    .insert({
      user_id: id[0],
      description: 'test',
      profile_picture_url: 'test',
      compensation: 'test',
      alias: 'test'
    })
  )
}
