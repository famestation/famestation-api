const userTable = 'users'
const companyTable = 'companies'
const hasher = require('../../src/utils/hasher')

exports.seed = function (knex, Promise) {
  return hasher.hash('qweqwe')
  .then((hash) =>
    knex(userTable).insert({
      firstname: 'test',
      lastname: 'test',
      email: 'company@test.com',
      password: hash,
      type: 'company'
    })
    .returning('id')
  )
  .then((id) =>
    knex(companyTable)
    .insert({
      user_id: id[0],
      company_name: 'test',
      country: 'test',
      type: 'test',
      company_website: 'test',
      description: 'test'
    })
  )
}
