const tableName = 'categories'

exports.seed = function (knex, Promise) {
  return Promise.all([
    knex(tableName).insert({ id: 1, name: 'Djur' }),
    knex(tableName).insert({ id: 2, name: 'Friluftsliv' }),
    knex(tableName).insert({ id: 3, name: 'Film' }),
    knex(tableName).insert({ id: 4, name: 'Föräldraskap' }),
    knex(tableName).insert({ id: 5, name: 'Hygien' }),
    knex(tableName).insert({ id: 6, name: 'Hälsa' }),
    knex(tableName).insert({ id: 7, name: 'Inredning' }),
    knex(tableName).insert({ id: 8, name: 'Konst' }),
    knex(tableName).insert({ id: 9, name: 'Livsstil' }),
    knex(tableName).insert({ id: 10, name: 'Mat' }),
    knex(tableName).insert({ id: 11, name: 'Miljö' }),
    knex(tableName).insert({ id: 12, name: 'Mode' }),
    knex(tableName).insert({ id: 13, name: 'Musik' }),
    knex(tableName).insert({ id: 14, name: 'Politik' }),
    knex(tableName).insert({ id: 15, name: 'Samhälle' }),
    knex(tableName).insert({ id: 16, name: 'Skönhet' }),
    knex(tableName).insert({ id: 17, name: 'Teknik' }),
    knex(tableName).insert({ id: 18, name: 'Träning' }),
    knex(tableName).insert({ id: 19, name: 'Utbildning' })
  ])
}
