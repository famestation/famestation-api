const errorResponse = require('../responses/errorResponse')
const ValidationError = require('../errors/ValidationError')

/**
 * A function which takes in a schema as a parameter
 * and returns a express function closure.
 */
function validateRequest (schema) {
  return (req, res, next) => {
    req.check(schema)

    return req.getValidationResult()
    .then(result => {
      if (result.isEmpty()) {
        return next()
      }
      return errorResponse(res, new ValidationError(result.array()))
    })
  }
}

module.exports = validateRequest
