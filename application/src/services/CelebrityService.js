const Celebrity = require('../models/Celebrity')
const SocialMediaLink = require('../models/SocialMediaLink')
const NotFoundError = require('../errors/NotFoundError')

const CelebrityService = {

  getAll () {
    return Celebrity.fetchAll({withRelated: ['user', 'categories', 'media_links']})
  },

  get (id) {
    return Celebrity.where({id}).fetch({withRelated: ['user', 'categories', 'media_links']})
    .then(user => {
      if (user) return user
      throw new NotFoundError('Celebrity not found', 'E_NOT_FOUND', 404)
    })
  },

  create (data, userId) {
    return new Celebrity({
      user_id: userId,
      description: data.description,
      profile_picture_url: data.profile_picture_url,
      compensation: data.compensation,
      alias: data.alias
    }).save()
  },

  insertMediaLinks (celebrityId, url) {
    return new SocialMediaLink({
      celebrity_id: celebrityId,
      url: url
    }).save()
  },

  update (id, data) {
    let userId
    return this.get(id)
    .then(dbUser => {
      userId = dbUser.attributes.user_id
      return Celebrity.forge({id: id})
    })
    .then(user => user.save({
      description: data.description,
      profile_picture_url: data.profile_picture_url,
      compensation: data.compensation,
      alias: data.alias
    }))
    .then(_ => userId)
  }

}

module.exports = CelebrityService
