const jwt = require('jsonwebtoken')
const moment = require('moment')
const hasher = require('../utils/hasher')
const User = require('../models/User')
const Celebrity = require('../models/Celebrity')
const Company = require('../models/Company')
const NotFoundError = require('../errors/NotFoundError')
const ValidationError = require('../errors/ValidationError')

/**
 * These functions creates a unix timestamp which points to 1hours ahead of time when created,
 * and current timestamp.
 **/
const inTwoHours = (m) => m(Date.now()).add(2, 'hours').unix()
const now = (m) => m(Date.now()).unix()

/**
 * The options field for the JWT token, more information can be found here:
 * https://tools.ietf.org/html/draft-jones-json-web-token-07#section-4.1
 **/
const jwtOptions = (id, uid, type) => ({
  _id: id,
  _uid: uid,
  _type: type,
  iat: now(moment),
  exp: inTwoHours(moment),
  iss: process.env.JWT_ISSUER
})

/**
 * Simple and self explanatory function.
 **/
function isVerifiedUser (user) {
  return user.attributes.verified
}

function getType (user) {
  if(user.attributes.type === 'celebrity'){
    return Celebrity.forge({
      user_id: user.id
    }).fetch()
  }
  return Company.forge({
    user_id: user.id
  }).fetch()
}

const AuthService = {

  verifyUser (email, password) {
    return User.where({email}).fetch()
    .then(user => {
      if (user) {
        if (isVerifiedUser(user)) {
          return hasher.compare(password, user.get('password'))
          .then(res => user)
        }
        throw new ValidationError({msg: 'E_USER_IS_NOT_VERIFIED'})
      }
      throw new NotFoundError({msg: 'E_USER_NOT_FOUND'})
    })
  },

  generateToken (user) {
    return getType(user)
    .then(type => {
      const options = jwtOptions(type.id, user.id, user.attributes.type)
      return new Promise((resolve, reject) => {
        jwt.sign(options, process.env.JWT_SECRET_KEY, {algorithm: 'HS256'}, (err, token) => {
          if (err) return reject('Token error.')
          resolve(token)
        })
      })
    })
  }

}

module.exports = AuthService
