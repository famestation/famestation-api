const Chat = require('../models/Chat').Chat
const ChatParticipants = require('../models/Chat').ChatParticipants
const ChatMessages = require('../models/Chat').ChatMessages
const NotFoundError = require('../errors/NotFoundError')
const uuidV4 = require('uuid/v4')
const sillyName = require('sillyname')

const ChatService = {

  get (uuid) {
    return Chat.where({uuid}).orderBy('created_at', 'DESC').fetch({withRelated: ['messages.user']})
    .then(conversation => {
      if (conversation) return conversation
      throw new NotFoundError('Conversation does not exist', 'E_NOT_FOUND', 404)
    })
  },

  allConversationsForUser (userId) {
    return ChatParticipants.where({ user_id: userId }).orderBy('created_at', 'DESC').fetchAll({withRelated: ['conversation.users']})
    .then(conversations => {
      if (conversations) return conversations.toJSON({ omitPivot: true })
      throw new NotFoundError('Conversation does not exist', 'E_NOT_FOUND', 404)
    })
  },

  createMessage (uuid, userId, message) {
    return this.get(uuid)
    .then((chat) => {
      return new ChatMessages({
        user_id: userId,
        conversation_id: chat.id,
        message: message
      }).save()
    })
  },

  create (data) {
    return new Chat({
      uuid: uuidV4(),
      channel: sillyName()
    }).save()
  },

  assignUsersToConversation (conversationId, userId) {
    return new ChatParticipants({
      user_id: userId,
      conversation_id: conversationId
    }).save()
  }

}

module.exports = ChatService
