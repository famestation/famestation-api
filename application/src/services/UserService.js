const User = require('../models/User')
const AlreadyExistsError = require('../errors/AlreadyExistsError')

const UserService = {

  getAll () {
    return User.fetchAll()
  },

  create (data, type) {
    return User.forge({
      email: data.email
    })
    .fetch()
    .then((user) => {
      if (user) throw new AlreadyExistsError({msg: 'E_EMAIL_ALREADY_EXISTS'})
      return new User({
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        password: data.password,
        type
      }).save()
    })
  },

  update (id, data) {
    return User.forge({id: id})
    .save({
      firstname: data.firstname,
      lastname: data.lastname,
      email: data.email,
      password: data.password
    })
  }

}

module.exports = UserService
