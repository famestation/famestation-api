const TypeEnum = require('../models/enums/UserTypeEnum')
const Category = require('../models/Category')
const CelebrityCategory = require('../models/CelebrityCategory')
const CompanyCategory = require('../models/CompanyCategory')

const CategoryService = {

  all () {
    return Category.fetchAll()
  },

  upsertRelation ({ categories }, modelId, type) {
    if (type === TypeEnum.celebrity) {
      return Promise.all(
        categories.map(category => new CelebrityCategory({
          category_id: category,
          celebrity_id: modelId
        }).save())
      )
      .then(results => results)
    }
    if (type === TypeEnum.company) {
      return Promise.all(
        categories.map(category => new CompanyCategory({
          category_id: category,
          company_id: modelId
        }).save())
      )
      .then(results => results)
    }
  }

}

module.exports = CategoryService




