const Company = require('../models/Company')
const NotFoundError = require('../errors/NotFoundError')

const CompanyService = {

  getAll () {
    return Company.fetchAll({withRelated: ['user', 'categories']})
  },

  get (id) {
    return Company.where({id}).fetch({withRelated: ['user', 'categories']})
    .then(company => {
      if (company) return company
      throw new NotFoundError({msg: 'E_COMPANY_NOT_FOUND'})
    })
  },

  create (data, userId) {
    return new Company({
      user_id: userId,
      company_name: data.company_name,
      country: data.country,
      type: data.type,
      company_website: data.company_website,
      description: data.description
    }).save()
  },

  update (id, data) {
    let userId
    return this.get(id)
    .then(dbUser => {
      userId = dbUser.attributes.user_id
      return Company.forge({id: id})
    })
    .then(dbCompany => dbCompany.save({
      company_name: data.company_name,
      country: data.country,
      type: data.type,
      company_website: data.company_website,
      description: data.description
    }))
    .then(_ => userId)
  }

}

module.exports = CompanyService
