const TypeEnum = require('../models/enums/UserTypeEnum')
const CompanyService = require('../services/CompanyService')
const CategoryService = require('../services/CategoryService')
const UserService = require('../services/UserService')
const ChatService = require('../services/ChatService')
const successResponse = require('../responses/successResponse')
const errorResponse = require('../responses/errorResponse')

const CompanyController = {

  all (req, res) {
    return CompanyService.getAll()
    .then(companies => successResponse(res, companies))
    .catch(e => errorResponse(res, e))
  },

  get (req, res) {
    return CompanyService.get(req.params.id)
    .then(company => successResponse(res, company))
    .catch(e => errorResponse(res, e))
  },

  create (req, res) {
    let company
    return UserService.create(req.body, TypeEnum.company)
    .then(createdUser => CompanyService.create(req.body, createdUser.id))
    .then(createdCompany => {
      company = createdCompany
      return CategoryService.upsertRelation(req.body, createdCompany.id, TypeEnum.company)
    })
    .then(relations => successResponse(res, company, 201))
    .catch(err => errorResponse(res, err))
  },

  update (req, res) {
    return CompanyService.update(req.params.id, req.body)
    .then(userId => {
      return UserService.update(userId, req.body)
    })
    .then(result => successResponse(res, null, 204))
    .catch(error => errorResponse(res, error))
  },

  getConversations (req, res) {
    return CompanyService.get(req.params.id)
    .then(company => ChatService.allConversationsForUser(company.attributes.user_id))
    .then(result => successResponse(res, result, 200))
    .catch(error => errorResponse(res, error))
  }

}

module.exports = CompanyController
