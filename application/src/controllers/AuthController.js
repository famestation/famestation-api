const AuthService = require('../services/AuthService')
const successResponse = require('../responses/successResponse')
const errorResponse = require('../responses/errorResponse')

const AuthController = {
  login (req, res) {
    const { email, password } = req.body

    return AuthService.verifyUser(email, password)
    .then(user => AuthService.generateToken(user))
    .then(token => successResponse(res, token))
    .catch(e => errorResponse(res, e))
  }
}

module.exports = AuthController
