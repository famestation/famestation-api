const CategoryService = require('../services/CategoryService')
const successResponse = require('../responses/successResponse')
const errorResponse = require('../responses/errorResponse')

const CategoryController = {
  all (req, res) {
    return CategoryService.all()
    .then(categories => successResponse(res, categories))
    .catch(e => errorResponse(res, e))
  }
}

module.exports = CategoryController
