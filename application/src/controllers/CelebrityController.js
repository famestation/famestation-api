const TypeEnum = require('../models/enums/UserTypeEnum')
const CelebrityService = require('../services/CelebrityService')
const ChatService = require('../services/ChatService')
const CategoryService = require('../services/CategoryService')
const UserService = require('../services/UserService')
const successResponse = require('../responses/successResponse')
const errorResponse = require('../responses/errorResponse')

const CelebrityController = {

  all (req, res) {
    return CelebrityService.getAll()
    .then(users => successResponse(res, users))
    .catch(e => errorResponse(res, e))
  },

  get (req, res) {
    return CelebrityService.get(req.params.id)
    .then(user => successResponse(res, user))
    .catch(e => errorResponse(res, e))
  },

  create (req, res) {
    let celebrity
    return UserService.create(req.body, TypeEnum.celebrity)
    .then(createdUser => CelebrityService.create(req.body, createdUser.id))
    .then(createdCelebrity => {
      celebrity = createdCelebrity
      return CategoryService.upsertRelation(req.body, createdCelebrity.id, TypeEnum.celebrity)
    })
    .then(relations => Promise.all(req.body.social_media_links.map((link) => CelebrityService.insertMediaLinks(celebrity.id, link.url))))
    .then(result => successResponse(res, celebrity, 201))
    .catch(err => errorResponse(res, err))
  },

  update (req, res) {
    return CelebrityService.update(req.params.id, req.body)
    .then(userId => UserService.update(userId, req.body))
    .then(result => successResponse(res, null, 204))
    .catch(error => errorResponse(res, error))
  },

  getConversations (req, res) {
    return CelebrityService.get(req.params.id)
    .then(celebrity => ChatService.allConversationsForUser(celebrity.attributes.user_id))
    .then(result => successResponse(res, result, 200))
    .catch(error => errorResponse(res, error))
  }

}

module.exports = CelebrityController
