const ChatService = require('../services/ChatService')
const successResponse = require('../responses/successResponse')
const errorResponse = require('../responses/errorResponse')

const ChatController = {

  get (req, res) {
    return ChatService.get(req.params.conversationId)
    .then(conversation => successResponse(res, conversation))
    .catch(e => errorResponse(res, e))
  },

  create (req, res) {
    return ChatService.create(req.body)
    .then(conversation => {
      return Promise.all([
        ChatService.assignUsersToConversation(conversation.id, req.body.starter),
        ChatService.assignUsersToConversation(conversation.id, req.body.receiver)
      ])
    })
    .then(conversation => successResponse(res, conversation, 201))
    .catch(e => errorResponse(res, e))
  },

  createMessage (req, res) {
    return ChatService.createMessage(req.params.conversationId, req.body.user, req.body.message)
    .then(message => successResponse(res, message, 201))
    .catch(e => errorResponse(res, e))
  }
}

module.exports = ChatController
