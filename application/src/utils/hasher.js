const bcrypt = require('bcrypt-nodejs')
const InvalidPasswordError = require('../errors/InvalidPasswordError')

const hasher = {

  hash (password) {
    const rounds = Number(process.env.AUTH_SALT_ROUNDS, 10)

    return new Promise((resolve, reject) =>
      bcrypt.genSalt(rounds, (err, salt) => {
        if (err) return reject(err)
        bcrypt.hash(password, salt, null, (err, hash) => {
          if (err) return reject(err)
          return resolve(hash)
        })
      })
    )
  },

  compare (string, hash) {
    return new Promise((resolve, reject) =>
      bcrypt.compare(string, hash, (err, result) => {
        if (err) return reject(new Error(err))
        return result ? resolve(result) : reject(new InvalidPasswordError({msg: 'E_INCORRECT_PASSWORD'}))
      })
    )
  }

}

module.exports = hasher
