const express = require('express')
const router = express.Router()

const getRouter = () => router

module.exports = getRouter
