const winston = require('winston')

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
      colorize: true,
      timestamp: true,
      prettyPrint: true,
      lavel: 'express-server'
    })
  ]
})

logger.stream = {
  write: message => logger.info(message)
}

module.exports = logger
