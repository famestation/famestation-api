const dbConfig = require('../../config/database')
const knex = require('knex')(dbConfig)
const bookshelf = require('bookshelf')(knex)

bookshelf.plugin('registry')
bookshelf.plugin('visibility')

module.exports = bookshelf

