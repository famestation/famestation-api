module.exports = function (error, httpCode) {
  return {
    http_code: error.httpCode || error,
    error_code: error.code,
    error_message: Array.isArray(error.errorMessage) ? error.errorMessage : [error.errorMessage]
  }
}
