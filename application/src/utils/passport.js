const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const User = require('../models/User')

// const facebookOptions = {
//   clientID: process.env.FACEBOOK_APP_ID,
//   clientSecret: process.env.FACEBOOK_APP_SECRET,
//   callbackURL: process.env.FACEBOOK_APP_CALLBACK_URL,
//   profileFields: ['id', 'email', 'first_name', 'last_name', 'locale', 'link'],
//   enableProof: true
// }

// passport.use('fb', new FacebookStrategy(facebookOptions, (accessToken, refreshToken, profile, done) => {
//   User.where({facebook_id: profile.id}).fetch()
//   .then((user) => {
//     if (user) {
//       return done(null, user.toJSON())
//     }
//     return User.forge({
//       firstname: profile.name.givenName,
//       lastname: profile.name.familyName,
//       facebook_id: profile.id,
//       email: profile.email
//     })
//     .save()
//     .then(function (user) {
//       done(null, user.toJSON())
//     })
//   })
//   done(null, null)
// }))

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
  secretOrKey: process.env.JWT_SECRET_KEY,
  issuer: process.env.JWT_ISSUER
}

passport.use('jwt', new JwtStrategy(jwtOptions, function (jwtPayload, done) {
  const id = jwtPayload._uid
  return User.where({id})
  .fetch()
  .then(user => {
    done(null, user)
    // WTF, null? See: http://goo.gl/rRqMUw
    return null
  })
  .catch(err => done(err, false))
}))

module.exports = passport
