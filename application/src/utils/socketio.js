const ChatService = require('../services/ChatService')

module.exports = function (io) {
  io.on('connection', socket => {
    socket.on('subscribe', (room) => {
      socket.join(room)
    })
    socket.on('send message', (data) => {
      ChatService.createMessage(data.uuid, data.user, data.message)
      io.sockets.in(data.uuid).emit('conversation private post', {
        data: data
      })
    })
  })
}
