function InvalidPasswordError (message, code = 'C_INCORRECT_PASSWORD', httpCode = 401) {
  this.name = 'InvalidPasswordError'
  this.errorMessage = message
  this.code = code
  this.httpCode = httpCode
  this.stack = (new Error()).stack
}

InvalidPasswordError.prototype = Object.create(Error.prototype)
InvalidPasswordError.prototype.constructor = InvalidPasswordError

module.exports = InvalidPasswordError
