function ValidationError (message, code = 'C_VALIDATION', httpCode = 422) {
  this.name = 'ValidationError'
  this.errorMessage = message
  this.code = code
  this.httpCode = httpCode
  this.stack = (new Error()).stack
}

ValidationError.prototype = Object.create(Error.prototype)
ValidationError.prototype.constructor = ValidationError

module.exports = ValidationError
