function AlreadyExistsError (message, code = 'C_DUPLICATE_RESOURCE', httpCode = 409) {
  this.name = 'AlreadyExistsError'
  this.errorMessage = message
  this.code = code
  this.httpCode = httpCode
  this.stack = (new Error()).stack
}

AlreadyExistsError.prototype = Object.create(Error.prototype)
AlreadyExistsError.prototype.constructor = AlreadyExistsError

module.exports = AlreadyExistsError
