function NotFoundError (message, code = 'C_RESOURCE_NOT_FOUND', httpCode = 404) {
  this.name = 'NotFoundError'
  this.errorMessage = message
  this.code = code
  this.httpCode = httpCode
  this.stack = (new Error()).stack
}

NotFoundError.prototype = Object.create(Error.prototype)
NotFoundError.prototype.constructor = NotFoundError

module.exports = NotFoundError
