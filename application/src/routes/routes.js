const router = require('../utils/router')()

router.use('/auth', require('./auth'))
router.use('/celebrities', require('./celebrities'))
router.use('/companies', require('./companies'))
router.use('/categories', require('./categories'))
router.use('/conversations', require('./conversations'))

module.exports = router
