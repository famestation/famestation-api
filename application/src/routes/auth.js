const express = require('express')
const passport = require('../utils/passport')
const router = express.Router()
const AuthController = require('../controllers/AuthController')
const validateRequest = require('../middlewares/validator')
const schema = require('./validation-schemas/auth')

router.post('/login', validateRequest(schema), AuthController.login)
router.get('/login/facebook', passport.authenticate('facebook'))
router.get('/login/facebook/cb',
  passport.authenticate('facebook', {scope: ['email']}),
  (req, res) => res.json({
    status: 'ok'
  })
)

module.exports = router
