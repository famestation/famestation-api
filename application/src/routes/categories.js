const express = require('express')
const router = express.Router()

const CategoriesController = require('../controllers/CategoriesController')

router.get('/', CategoriesController.all)

module.exports = router
