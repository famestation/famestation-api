module.exports = {
  'firstname': {
    notEmpty: true,
    errorMessage: 'E_MISSING_FIRSTNAME_FIELD'
  },
  'lastname': {
    notEmpty: true,
    errorMessage: 'E_MISSING_LASTNAME_FIELD'
  },
  'email': {
    notEmpty: true,
    errorMessage: 'E_MISSING_EMAIL_FIELD'
  },
  'password': {
    notEmpty: true,
    errorMessage: 'E_MISSING_PASSWORD_FIELD'
  },
  'type': {
    notEmpty: true,
    errorMessage: 'E_MISSING_TYPE_FIELD'
  },
  'company_name': {
    notEmpty: true,
    errorMessage: 'E_MISSING_COMPANY_NAME_FIELD'
  }
}
