module.exports = {
  'email': {
    notEmpty: true,
    errorMessage: 'E_MISSING_EMAIL_FIELD'
  },
  'password': {
    notEmpty: true,
    errorMessage: 'E_MISSING_PASSWORD_FIELD'
  }
}
