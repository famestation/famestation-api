const express = require('express')
const router = express.Router()
const auth = require('../utils/passport')
const validateRequest = require('../middlewares/validator')
const schema = require('./validation-schemas/companies')

const CompanyController = require('../controllers/CompanyController')

router.post('/', validateRequest(schema), CompanyController.create)

router.use('*', auth.authenticate('jwt', {session: false}))
router.get('/', CompanyController.all)
router.get('/:id', CompanyController.get)
router.put('/:id', CompanyController.update)

router.get('/:id/conversations', CompanyController.getConversations)

module.exports = router
