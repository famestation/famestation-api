const express = require('express')
const router = express.Router()
const auth = require('../utils/passport')

const ChatController = require('../controllers/ChatController')

router.use('*', auth.authenticate('jwt', {session: false}))
router.post('/', ChatController.create)
router.get('/:conversationId', ChatController.get)
router.post('/:conversationId', ChatController.createMessage)


module.exports = router
