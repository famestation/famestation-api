const express = require('express')
const router = express.Router()
const auth = require('../utils/passport')
const validateRequest = require('../middlewares/validator')
const schema = require('./validation-schemas/celebrities')

const CelebrityController = require('../controllers/CelebrityController')

router.post('/', validateRequest(schema), CelebrityController.create)

router.use('*', auth.authenticate('jwt', {session: false}))
router.get('/', CelebrityController.all)
router.get('/:id', CelebrityController.get)
router.put('/:id', CelebrityController.update)

router.get('/:id/conversations', CelebrityController.getConversations)

module.exports = router
