const bookshelf = require('../utils/bookshelf')

const SocialMediaLink = bookshelf.Model.extend({
  tableName: 'celebrity_social_media_links',
  hasTimestamps: true,
  hidden: ['created_at', 'updated_at', 'id']
})

module.exports = bookshelf.model('SocialMediaLink', SocialMediaLink)
