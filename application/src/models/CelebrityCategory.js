const bookshelf = require('../utils/bookshelf')

const CelebrityCategory = bookshelf.Model.extend({
  tableName: 'celebrity_categories',
  hasTimestamps: true,
  hidden: ['created_at', 'updated_at']
})

module.exports = bookshelf.model('CelebrityCategory', CelebrityCategory)
