const bookshelf = require('../utils/bookshelf')
const User = require('./User')
const Category = require('./Category')
const CompanyCategory = require('./CompanyCategory')

const Company = bookshelf.Model.extend({
  tableName: 'companies',
  hasTimestamps: true,
  hidden: ['created_at', 'updated_at', 'user_id'],
  user: function () {
    return this.belongsTo(User)
  },

  categories: function () {
    return this.hasMany(Category).through(CompanyCategory, 'id', null, 'category_id')
  }
})

module.exports = bookshelf.model('Company', Company)
