const bookshelf = require('../utils/bookshelf')
const User = require('./User')
const CelebrityCategory = require('./CelebrityCategory')
const Category = require('./Category')
const SocialMediaLink = require('./SocialMediaLink')

const Celebrity = bookshelf.Model.extend({
  tableName: 'celebrities',
  hasTimestamps: true,
  hidden: ['created_at', 'updated_at', 'user_id'],

  user: function () {
    return this.belongsTo(User)
  },

  categories: function () {
    return this.hasMany(Category).through(CelebrityCategory, 'id', null, 'category_id')
  },

  media_links: function () {
    return this.hasMany(SocialMediaLink)
  }
})

module.exports = bookshelf.model('Celebrity', Celebrity)
