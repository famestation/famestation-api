const bookshelf = require('../utils/bookshelf')

const CompanyCategory = bookshelf.Model.extend({
  tableName: 'company_categories',
  hasTimestamps: true,
  hidden: ['created_at', 'updated_at']
})

module.exports = bookshelf.model('CompanyCategory', CompanyCategory)
