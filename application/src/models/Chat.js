const bookshelf = require('../utils/bookshelf')
const User = require('./User')

const Chat = bookshelf.Model.extend({
  tableName: 'chat',
  hasTimestamps: true,
  hidden: ['created_at', 'id'],

  messages: function () {
    return this.hasMany(ChatMessages, 'conversation_id')
  },

  users: function () {
    return this.belongsToMany(User).through(ChatParticipants, 'conversation_id')
  }

})

const ChatMessages = bookshelf.Model.extend({
  tableName: 'chat_messages',
  hasTimestamps: true,
  hidden: ['updated_at', 'id', 'conversation_id', 'user_id'],

  messages: function () {
    return this.belongsTo(Chat)
  },

  user: function () {
    return this.belongsTo(User)
  }

})

const ChatParticipants = bookshelf.Model.extend({
  tableName: 'chat_participants',
  hasTimestamps: true,
  hidden: ['updated_at', 'id', 'conversation_id', 'user_id'],

  conversation: function () {
    return this.belongsTo(Chat, 'conversation_id')
  }

})

module.exports = {
  Chat: bookshelf.model('Chat', Chat),
  ChatMessages: bookshelf.model('ChatMessages', ChatMessages),
  ChatParticipants: bookshelf.model('ChatParticipants', ChatParticipants)
}
