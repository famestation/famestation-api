const bookshelf = require('../utils/bookshelf')
const hasher = require('../utils/hasher')

const User = bookshelf.Model.extend({
  tableName: 'users',
  hasTimestamps: true,
  hidden: ['created_at', 'updated_at', 'password'],

  initialize: function () {
    this.on('saving', this.hashPassword, this)
  },

  hashPassword: function (model, attrs, options) {
    return hasher.hash(model.attributes.password)
    .then(hash => model.set('password', hash))
  }
})

module.exports = bookshelf.model('User', User)
