const bookshelf = require('../utils/bookshelf')

const Category = bookshelf.Model.extend({
  tableName: 'categories',
  hasTimestamps: true,
  hidden: ['created_at', 'updated_at']
})

module.exports = bookshelf.model('Category', Category)
