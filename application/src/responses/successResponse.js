module.exports = (res, data = 'Ok', statuscode = 200) => {
  const status = true
  res.status(statuscode)
  .json({
    success: status,
    result: data
  })
}
