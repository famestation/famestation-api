const errorBuilder = require('../utils/errorBuilder')

module.exports = (res, error, statuscode = 400) => {
  const status = false
  error = errorBuilder(error, statuscode)
  res.status(error.http_code || statuscode)
  .json({
    success: status,
    result: error
  })
}
