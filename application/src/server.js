const express = require('express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const passport = require('passport')
const cors = require('cors')
const expressValidator = require('express-validator')

const routes = require('./routes/routes')

/**
 * Initialize express.
 */
const app = express()

/**
 * Defines all the middlewares the application uses.
 */
app.use(cors())
app.use(helmet())
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(passport.initialize())
app.use(expressValidator())

/**
 * Defines all the route the application uses.
 */
app.use('/v1', routes)

/**
 * Catches any errors that have occucred in the app.
 */
app.use((err, req, res, next) => {
  return res.status(500).send()
})

module.exports = app
