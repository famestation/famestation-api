require('dotenv').load()
const server = require('./server')
const logger = require('./utils/logger')

// Initialize Socket.io
const http = require('http').createServer(server)
const io = require('socket.io')(http)
require('./utils/socketio')(io)

http.listen(process.env.PORT || 3000, function () {
  const host = this.address().address
  const port = this.address().port
  logger.info(`Listening at http://${host}:${port}`)
})
