module.exports = {
  get (attr) {
    const attribute = attr || {}
    return {
      firstname: attribute.firstname || 'Sven',
      lastname: attribute.lastname || 'Olsson',
      email: attribute.email || 'sven.olsson@test.se',
      password: attribute.password || 'test',
      description: attribute.description || 'Test description',
      profile_picture_url: attribute.profile_picture_url || 'http://www.google.com.jpg',
      compensation: attribute.compensation || 'cash',
      alias: attribute.alias || 'Märta3000',
      categories: attribute.categories || [1, 3, 3, 7],
      social_media_links: attribute.categories || [{
        url: 'instagram'
      },
      {
        url: 'twitter'
      },
      {
        url: 'youtube'
      }]
    }
  }
}
