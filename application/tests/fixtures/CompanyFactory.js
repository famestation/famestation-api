module.exports = {
  get (attr) {
    const attribute = attr || {}
    return {
      firstname: attribute.firstname || 'Nick',
      lastname: attribute.lastname || 'Svensson',
      email: attribute.email || 'nick.svensson@test.se',
      password: attribute.password || 'test',
      company_name: attribute.description || 'Company Inc',
      country: attribute.country || 'Sweden',
      type: attribute.type || 'Online shop',
      company_website: attribute.company_website || 'https://www.company.com',
      description: attribute.description || 'Test description',
      categories: attribute.categories || [1, 3, 3, 7]
    }
  }
}
