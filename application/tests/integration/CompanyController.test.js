const request = require('supertest')
const server = require('../../src/server')
const factory = require('../fixtures/CompanyFactory')
const authRequest = require('../auth-helper')

describe('CompanyController', function () {
  let token
  const user = {
    email: 'company@test.com',
    password: 'qweqwe'
  }
  before(() =>
    authRequest(request, server, user)
    .then(({body}) => token = body.result)
  )

  describe('#Create#', () => {
    it('Should give back an error when sending in nothing', () =>
      request(server)
      .post('/v1/companies')
      .send({})
      .expect(422)
      .then(({ body }) => {
        body.success.should.equal(false)
        body.result.error_message[0].msg.should.equal('E_MISSING_FIRSTNAME_FIELD')
        body.result.error_message[1].msg.should.equal('E_MISSING_LASTNAME_FIELD')
        body.result.error_message[2].msg.should.equal('E_MISSING_EMAIL_FIELD')
        body.result.error_message[3].msg.should.equal('E_MISSING_PASSWORD_FIELD')
        body.result.error_message[4].msg.should.equal('E_MISSING_TYPE_FIELD')
        body.result.error_message[5].msg.should.equal('E_MISSING_COMPANY_NAME_FIELD')
      })
    )

    it('Should successfully create a celebrity', () =>
      request(server)
      .post('/v1/companies')
      .send(factory.get())
      .expect(201)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.should.be.a.Object
      })
    )


    it('Should give back an error if trying to register a company with an existing email', () =>
      request(server)
      .post('/v1/companies')
      .send(factory.get())
      .expect(409)
      .then(({body}) => {
        body.success.should.equal(false)
        body.result.error_code.should.equal('C_DUPLICATE_RESOURCE')
        body.result.error_message[0].msg.should.equal('E_EMAIL_ALREADY_EXISTS')
      })
    )
  })

  describe('#List companies#', () => {
    it('Should give back authentication error if no token is provided', () =>
      request(server)
      .get('/v1/companies')
      .expect(401)
    )

    it('Should list multiple companies', () =>
      request(server)
      .get('/v1/companies')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .then(({body}) => {
        body.success.should.equal(true)
        body.result.should.be.an.array
      })
    )

    it('Should list a single company', () =>
      request(server)
      .get('/v1/companies/1')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .then(({body}) => {
        body.success.should.equal(true)
        body.result.company_name.should.equal('test')
        body.result.country.should.equal('test')
        body.result.type.should.equal('test')
        body.result.company_website.should.equal('test')
        body.result.description.should.equal('test')
        body.result.user.firstname.should.equal('test')
        body.result.user.lastname.should.equal('test')
        body.result.user.email.should.equal('company@test.com')
        body.result.user.type.should.equal('company')
        body.result.user.verified.should.equal(true)
      })
    )
  })

  describe('#Update company#', () => {
    it('Should update a company', () =>
      request(server)
      .put('/v1/companies/1')
      .set('Authorization', `Bearer ${token}`)
      .send(factory.get({
        company_name: 'testUpdate',
        country: 'testUpdate',
        type: 'testUpdate',
        company_website: 'testUpdate',
        description: 'testUpdate',
        firstname: 'testUpdate',
        lastname: 'testUpdate',
        email: 'testUpdate2'
      }))
      .expect(204)
    )
  })
  describe('#Delete company#', () => {
    it.skip('Should delete a company', () =>
      request(server)
      .delete('/v1/companies')
      .expect(200)
    )
  })
})
