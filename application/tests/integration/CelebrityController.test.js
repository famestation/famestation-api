const request = require('supertest')
const server = require('../../src/server')
const factory = require('../fixtures/CelebrityFactory')
const authRequest = require('../auth-helper')

describe('CelebrityController', function () {
  let token
  const user = {
    email: 'celebrity@test.com',
    password: 'qweqwe'
  }
  before(() =>
    authRequest(request, server, user)
    .then(({body}) => token = body.result)
  )
  describe('#Create#', () => {
    it('Should give back an error when sending in nothing', () =>
      request(server)
      .post('/v1/celebrities')
      .send({})
      .expect(422)
      .then(({ body }) => {
        body.success.should.equal(false)
        body.result.error_message[0].msg.should.equal('E_MISSING_FIRSTNAME_FIELD')
        body.result.error_message[1].msg.should.equal('E_MISSING_LASTNAME_FIELD')
        body.result.error_message[2].msg.should.equal('E_MISSING_EMAIL_FIELD')
        body.result.error_message[3].msg.should.equal('E_MISSING_PASSWORD_FIELD')
        body.result.error_message[4].msg.should.equal('E_MISSING_ALIAS_FIELD')
      })
    )

    it('Should successfully create a celebrity', () =>
      request(server)
      .post('/v1/celebrities')
      .send(factory.get())
      .expect(201)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.should.be.a.Object
      })
    )

    it('Should give back an error if trying to register with an existing email', () =>
      request(server)
      .post('/v1/celebrities')
      .send(factory.get())
      .expect(409)
      .then(({body}) => {
        body.success.should.equal(false)
        body.result.error_code.should.equal('C_DUPLICATE_RESOURCE')
        body.result.error_message[0].msg.should.equal('E_EMAIL_ALREADY_EXISTS')
      })
    )
  })

  describe('#List celebrities#', () => {
    it('Should give back an error if user isn\'t authenticated', () =>
      request(server)
      .get('/v1/celebrities')
      .expect(401)
    )

    it('Should give back a list of celebrities', () =>
      request(server)
      .get('/v1/celebrities')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.should.be.an('array').and.not.be.empty
      })
    )

    it('Should return a single celebrity', () =>
      request(server)
      .get('/v1/celebrities/1')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.description.should.equal('test')
        body.result.profile_picture_url.should.equal('test')
        body.result.compensation.should.equal('test')
        body.result.alias.should.equal('test')
        body.result.user.firstname.should.equal('test')
        body.result.user.lastname.should.equal('test')
        body.result.user.email.should.equal('celebrity@test.com')
        body.result.user.verified.should.equal(true)
        body.result.user.type.should.equal('celebrity')
      })
    )
  })

  describe('#Update celebrity#', () => {
    it('Should update a celebrity', () =>
      request(server)
      .put('/v1/celebrities/1')
      .set('Authorization', `Bearer ${token}`)
      .send(factory.get({
        description: 'testUpdate',
        profile_picture_url: 'testUpdate',
        compensation: 'testUpdate',
        alias: 'testUpdate',
        firstname: 'testUpdate',
        lastname: 'testUpdate',
        email: 'testUpdate'
      }))
      .expect(204)
    )
  })

  describe('#Delete celebrity#', () => {
    it.skip('Should delete a celebrity', () =>
      request(server)
      .delete('/v1/celebrities')
      .expect(200)
    )
  })
})
