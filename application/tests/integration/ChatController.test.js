const request = require('supertest')
const server = require('../../src/server')
const authRequest = require('../auth-helper')

describe.only('CelebrityController', function () {
  let token
  const user = {
    email: 'celebrity@test.com',
    password: 'qweqwe'
  }
  before(() =>
    authRequest(request, server, user)
    .then(({body}) => token = body.result)
  )

  describe('#Create#', () => {
    it('Should create a new chat between two users', () =>
      request(server)
      .post('/v1/conversations')
      .set('Authorization', `Bearer ${token}`)
      .send({
        starter: 1,
        receiver: 2
      })
      .expect(201)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.should.not.be.empty
      })
    )
  })

  describe('#List#', () => {
    let conversationUuid

    it('Should list all conversations a celebrity have', () =>
      request(server)
      .get('/v1/celebrities/1/conversations')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .then(({ body }) => {
        conversationUuid = body.result[0].conversation.uuid
        body.success.should.equal(true)
        body.result.should.not.be.empty
      })
    )

    it('Should list all messages in a conversation', () =>
      request(server)
      .get(`/v1/conversations/${conversationUuid}`)
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.should.not.be.empty
      })
    )
  })


  describe('#Create messages in a conversation#', () => {
    let conversationUuid
    before(() =>
      request(server)
      .get('/v1/celebrities/1/conversations')
      .set('Authorization', `Bearer ${token}`)
      .then(({ body }) => {
        conversationUuid = body.result[0].conversation.uuid
      })
    )

    it('Should create a message in a conversation', () =>
      request(server)
      .post(`/v1/conversations/${conversationUuid}`)
      .send({
        user: 1,
        message: 'test'
      })
      .set('Authorization', `Bearer ${token}`)
      .expect(201)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.should.not.be.empty
        body.result.message.should.equal('test')
      })
    )

    it('Should list all messages in a conversation', () =>
      request(server)
      .get(`/v1/conversations/${conversationUuid}`)
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.should.not.be.empty
      })
    )
  })
})
