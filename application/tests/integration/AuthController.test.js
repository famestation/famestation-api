const request = require('supertest')
const server = require('../../src/server')

describe('AuthController', function () {
  describe('#Login#', () => {
    it('Should give back an error if email and password is missing', () =>
      request(server)
      .post('/v1/auth/login')
      .send({})
      .expect(422)
      .then(({ body }) => {
        body.success.should.equal(false)
        body.result.error_message[0].msg.should.equal('E_MISSING_EMAIL_FIELD')
        body.result.error_message[1].msg.should.equal('E_MISSING_PASSWORD_FIELD')
      })
    )

    it('Should give back an error if user doesn\'t exists', () =>
      request(server)
      .post('/v1/auth/login')
      .send({
        email: 'non-existing@user.com',
        password: 'qweqwe'
      })
      .expect(404)
      .then(({ body }) => {
        body.success.should.equal(false)
        body.result.error_message[0].msg.should.equal('E_USER_NOT_FOUND')
      })
    )

    it('Should give back an error if user entered wrong password', () =>
      request(server)
      .post('/v1/auth/login')
      .send({
        email: 'celebrity@test.com',
        password: 'wrong-password'
      })
      .expect(401)
      .then(({ body }) => {
        body.success.should.equal(false)
        body.result.error_message[0].msg.should.equal('E_INCORRECT_PASSWORD')
      })
    )

    it('Should successfully login a user', () =>
      request(server)
      .post('/v1/auth/login')
      .send({
        email: 'celebrity@test.com',
        password: 'qweqwe'
      })
      .expect(200)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.should.be.a.String
        body.result.should.match(/[a-zA-Z0-9\-_]+?\.[a-zA-Z0-9\-_]+?\.([a-zA-Z0-9\-_]+)$/)
      })
    )
  })
})
