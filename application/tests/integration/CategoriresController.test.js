const request = require('supertest')
const server = require('../../src/server')

describe('CategoriesController', function () {
  describe('#List categorires#', () => {
    it('Should list all of the available categoreis', () =>
      request(server)
      .get('/v1/categories')
      .expect(200)
      .then(({ body }) => {
        body.success.should.equal(true)
        body.result.should.have.length.above(10).and.be.an.array
      })
    )
  })
})
