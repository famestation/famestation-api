module.exports = function (request, app, user) {
  return request(app)
    .post('/v1/auth/login')
    .send({
      email: user.email,
      password: user.password
    })
}
