require('dotenv').config({path: '.env-test'})
require('chai').should()
const dbConfig = require('../knexfile').test
const knex = require('knex')(dbConfig)

before(() => {
  return knex.migrate.rollback()
  .then(() =>
    knex.migrate.latest()
  )
  .then(() =>
    knex.seed.run()
  )
})

after(() => {
  console.log()
  console.log('Tests are completed, database is not rolledback.')
})
