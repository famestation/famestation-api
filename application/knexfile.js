require('dotenv').load()
const dbConfig = require('./config/database')

module.exports = {

  test: {
    client: 'postgresql',
    connection: dbConfig.connection,
    pool: {
      min: 1,
      max: 1
    },
    seeds: {
      directory: `${__dirname}/seeds/test`
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  development: {
    debug: true,
    client: 'postgresql',
    connection: dbConfig.connection,
    pool: {
      min: 1,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: dbConfig.connection,
    pool: {
      min: 4,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

}
