const tableName = 'users'

exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable(tableName, function (table) {
      table.increments('id').primary()
      table.string('firstname')
      table.string('lastname')
      table.string('email').unique()
      table.string('password')
      table.string('type')
      table.boolean('verified').defaultTo(true)
      table.timestamps()
    })
  ])
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable(tableName)
}
