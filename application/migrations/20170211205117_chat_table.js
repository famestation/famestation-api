const tableName = 'chat'

exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable(tableName, function (table) {
      table.increments('id').primary()
      table.string('uuid')
      table.string('channel')
      table.timestamps()
    })
  ])
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable(tableName)
}
