const tableName = 'chat_participants'

exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable(tableName, function (table) {
      table.increments('id').primary()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.integer('conversation_id').unsigned().references('id').inTable('chat')
      table.timestamps()
    })
  ])
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable(tableName)
}
