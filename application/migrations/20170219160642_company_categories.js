const tableName = 'company_categories'
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable(tableName, function (table) {
      table.increments('id').primary()
      table.integer('category_id').unsigned().references('id').inTable('categories')
      table.integer('company_id').unsigned().references('id').inTable('companies')
      table.timestamps()
    })
  ])
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable(tableName)
}
