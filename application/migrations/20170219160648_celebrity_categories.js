const tableName = 'celebrity_categories'
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable(tableName, function (table) {
      table.increments('id').primary()
      table.integer('category_id').unsigned().references('id').inTable('categories')
      table.integer('celebrity_id').unsigned().references('id').inTable('celebrities')
      table.timestamps()
    })
  ])
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable(tableName)
}
