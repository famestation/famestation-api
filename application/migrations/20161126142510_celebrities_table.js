const tableName = 'celebrities'

/**
 * Social media profiles.
 */
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable(tableName, function (table) {
      table.increments('id').primary()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.text('description')
      table.text('profile_picture_url')
      table.text('compensation')
      table.text('alias')
      table.timestamps()
    })
  ])
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable(tableName)
}
