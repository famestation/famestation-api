const tableName = 'categories'
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable(tableName, function (table) {
      table.increments('id').primary()
      table.string('name')
      table.timestamps()
    })
  ])
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable(tableName)
}
