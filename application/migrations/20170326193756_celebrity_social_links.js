const tableName = 'celebrity_social_media_links'

exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable(tableName, function (table) {
      table.increments('id').primary()
      table.integer('celebrity_id').unsigned().references('id').inTable('celebrities')
      table.string('url')
      table.timestamps()
    })
  ])
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable(tableName)
}
